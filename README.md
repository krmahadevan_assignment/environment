# Instructions.

## Pre-requisities

1. Maven
2. JDK17
3. Docker

## Clone the repositories

1. Manager: `https://gitlab.com/krmahadevan_assignment/manager`
2. Worker: `https://gitlab.com/krmahadevan_assignment/worker`
3. Proxy Server: `https://gitlab.com/krmahadevan_assignment/bmp_proxy`
4. Functional tests: `https://gitlab.com/krmahadevan_assignment/functional_tests`
5. This current repository: `https://gitlab.com/krmahadevan_assignment/environment`

## Bringing up the environment

* Build the Proxy server by running: `mvn clean package`.
* Spawn 2 instances of the proxy server by running the below commands:
  * `Proxy-Server` for the manager: 

```shell
java -Dproxy.port=4445 -Dserver.port=6666 -jar target/bmp_proxy-1.0-SNAPSHOT.jar
```

  * `Proxy-Server` for the worker: 

```shell
java -Dproxy.port=5556 -Dserver.port=6667 -jar target/bmp_proxy-1.0-SNAPSHOT.jar
```
* Bring up the infrastructure components by running the below command (from the directory wherein the environment codebase was cloned):

```shell
docker-compose -f docker-compose-infra.yaml up
```

* Now start the Manager process by running (in the directory where the manager codebase was cloned):

```shell
mvn clean test-compile spring-boot:run
```

* Now start the Worker process by running (in the directory wherein the worker codebase was cloned)


With this the environment should be running with the ports information as below:

* PostGreSQL database - **Port:** `5432`
* LocalStack AWS Simulation container - **Port:** `4566`
* Manager service - **Port:** `4444`
* Manager Proxy:
  * **Proxy server Port:** `4445`
  * **App   server Port:** `6666`
* Worker  service - **Port:** `5555`
* Worker Proxy:
  * **Proxy server Port:** `5556`
  * **App   server Port:** `6667`

## Running the functional tests

From the directory where the functional tests were cloned, run the command

```shell
mvn clean test
```   
