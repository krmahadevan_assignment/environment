# Functional automation test scenarios

## 1. Scenario

Manager has no worker nodes and a job scheduling is requested.

_Expected outcome:_ The job scheduling should get aborted with a proper reason code in the ledger table.

## 2. Scenario

There's no data to be injested. Manager wakes up and tries to schedule an ingestion.

_Expected outcome:_ Ingestion attempt should fail and there should be a log in the journal about this failed attempt.

## 3.Scenario

Manager schedules a job and worker starts ingesting it. Now block heartbeat.

_Expected outcome:_ Manager should remove the node from which there's no heart beat. But the job should be completed by the worker.


## 4. Scenario

Manager schedules an ingestion and worker successfully completes the ingestion.

_Expected outcome:_ The table should have a successful value for the file location in s3. Ensure that the object resides in s3 and is available for use.


## 5. Scenario

Manager schedules an ingestion. Worker now sends duplicate final status (first failed and then passed)

_Expected outcome:_ The ingestion job should be failed.

## 6. Scenario

Manager schedules an ingestion. Worker starts processing it. Block job status permanently (Simulating a stale job)

_Expected outcome:_ After a staleness period is over, the inflight job should have gotten marked as failed (due to it being stale, with a proper reason code)


## 7. Scenario

Manager schedules a job and worker starts ingesting it. Now block job status. After about 20 seconds, try scheduling another ingestion.

_Expected outcome:_ Manager should fail the inflight job. The next schedule should be rejected by the worker and it should NOT have any entries in the ledger.







